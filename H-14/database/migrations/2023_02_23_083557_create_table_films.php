<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_films', function (Blueprint $table) {
            $table->id();
            $table->string('judul', 45);
            $table->string('ringkasan', 255);
            $table->integer('tahun', 5);
            $table->string('poster', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_films');
    }
};
